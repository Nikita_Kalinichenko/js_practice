var month = 12;

 function getSeason(month) {
     if (month < 12 && month <=2 && month > 0 || month == 12){
        return "Зима"; 
     } else if (month >= 3 && month <=5 && month != 0){
        return "Весна"; 
     } else if (month >= 6 && month <=8 && month != 0){
        return "Лето";
     } else if (month >= 9 && month <=11 && month != 0){
        return "Осень";
     } else{
        return "Неверный номер месяца";
     }
 };