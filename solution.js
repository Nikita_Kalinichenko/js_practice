//Подсказка: сюда можно складывать записи адресной книги.
var phoneBook = [],
    lowerArr = []

//Здесь можно объявить переменные и функции, которые понядобятся вам для работы ваших функций
    resultArray = [];

module.exports = {
    getWords: function getWords(text){
      var arr = text.split(' '),
          result = [];

          for (var i = 0; i < arr.length; i++) {
            if (arr[i][0] == '#') {
              result.push(arr[i].slice(1));
          }
    }
    return result;
    },
    normalizeWords: function normalizeWords(words){
          for (var i = 0; i < words.length; i++) {
            lowerArr[i] = words[i].toLowerCase();
          }
          for (var j = 0; j < lowerArr.length - 1; j++) {
            for (var i = j + 1; i < lowerArr.length; i++) {
              if (lowerArr[i] === lowerArr[j]) {
                lowerArr.splice(i, 1);
                i--;
          }
          }
    }
    var result = lowerArr.join(', ');

    return result;
    },
    addressBook: function addressBook(command){
      var commanArray = command.split(' ');
          commandName = commanArray[0];

      if (commandName === 'ADD') {
      return addContact(command);
    }

      if (commandName === 'REMOVE_PHONE') {
      return removePhone(command);
    } 

      if (commandName === 'SHOW') {
      return show();
    }

// Команда добавления контакта
    function addContact() {
    var newName = commanArray[1];
        phoneNumbersArrayToAdd = commanArray[2].split(',');

      if (phoneBook.hasOwnProperty(newName)) {
        
      var initialPhonesArray = phoneBook[newName].split(', ');
          updatedPhoneList = initialPhonesArray.concat(phoneNumbersArrayToAdd);

          phoneBook[newName] = updatedPhoneList.join(', ');
          return;
    }
    phoneBook[newName] = phoneNumbersArrayToAdd.join(', ');
    }

// Команда удаления номера
    function removePhone() {
    var phoneToRemove = commanArray[1];
        clone = {};

    for (var key in phoneBook) {
      clone[key] = phoneBook[key];
    }

    for (var name in phoneBook) {
      if (phoneBook[name] == phoneToRemove) {
        delete phoneBook[name];
      } else if (phoneBook[name].split(', ').indexOf(phoneToRemove) != -1) {
        var initialPhonesArray = phoneBook[name].split(', ');
        for (var i = 0; i < initialPhonesArray.length; i++) {
          if (initialPhonesArray[i] == phoneToRemove) {
            initialPhonesArray.splice(i, 1);
          }
        }
        phoneBook[name] = initialPhonesArray.join(', ');
      }
    }
    return JSON.stringify(clone) === JSON.stringify(phoneBook) ? false : true;
    }

// Команда возвращения содержимого телефонной книги
    function show() {
      var arr = [];
          i = 0;
        for (var key in phoneBook) {
          arr[i] = key + ': ' + phoneBook[key];
          i++;
    }
    return arr.sort();
    }
      }
   }
