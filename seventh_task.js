// на оси y от -1 до 7
// на оси x от 1 до 9

var xCoordinate = 3,
    yCoordinate = 5,
    radius = 4;

    function isPointInCircle(x,y){
        if ((x-xCoordinate)*(x-xCoordinate)+(y-yCoordinate)*(y-yCoordinate) <= radius*radius){
           return true;
        }
        else{
           return false;
        }
    };