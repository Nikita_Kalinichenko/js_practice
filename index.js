/**
 * @param {Number} hours
 * @param {Number} minutes
 * @returns {Boolean}
 */
var xCoordinate = 3,
    yCoordinate = 5,
    radius = 4,
    x = -6,
    y = 2;

module.exports = {
    isValidTime: function() {},
    addMinutes: function (hours, minutes, add) {
     },
    getSeason: function(){},
    getDayDeclation: function(){},
    getSumm: function(){},
    isPointInCircle: function isPointInCircle(x,y){
        if ((x-xCoordinate)*(x-xCoordinate)+(y-yCoordinate)*(y-yCoordinate) <= radius*radius){
            return true;
        }
        else {
            return false;
        }
    },
    isPointInRectangle: function isPointInRectangle(x,y){
        if (y <= 4 / 7 * (x + 7) && y <= 3 * (1 - 0.2 * x) && (y >= 2 * (0.2 * x - 1)) && y >= (-1.5) * (x + 8)){
            return true;
        }
        else {
            return false;
        }
    }
}