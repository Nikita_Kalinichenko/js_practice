var hour = 12;
    minutes = 30;
    randomMin = 15;

 function addMinutes(hour,minutes,randomMin) {
    var newMinValue = (minutes + randomMin) % 60,
        newHourValue = (hour + Math.floor((minutes + randomMin)/60)) % 24;

     if (newHourValue < 10 && newMinValue < 10){
        return `0${newHourValue}:0${newMinValue}`; 
     } else if (newHourValue < 10){
        return `0${newHourValue}:${newMinValue}`;
     } else if (newMinValue < 10){
        return `${newHourValue}:0${newMinValue}`;  
     } else{
        return `${newHourValue}:${newMinValue}`;
     }
 };